﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

public class WeaponSystem : ComponentSystem
{
    protected override void OnCreate()
    {
        Entities.ForEach((ref WeaponData weaponData) =>
        {
            weaponData.spawnCountdown = 1f;
        });
    }

    protected override void OnUpdate()
    {
        Entities.ForEach((ref WeaponData weaponData) =>
        {
            weaponData.spawnCountdown -= Time.DeltaTime;

            if (weaponData.spawnCountdown <= 0f)
            {
                weaponData.spawnCountdown = weaponData.spawnTimer;

                for (int i = 0; i < weaponData.bulletCount; i++)
                {
                    Entity spawnedEntity = Entity.Null;
                    //if (UnityEngine.Random.value > .6f)
                    if (i % 2 == 0)
                        spawnedEntity = EntityManager.Instantiate(weaponData.orbDestroyable);
                    else
                        spawnedEntity = EntityManager.Instantiate(weaponData.orbUndestroyable);

                    EntityManager.SetComponentData(spawnedEntity,
                        new Translation { Value = new float3(0f, .35f, 0f) }
                    );

                    EntityManager.AddComponentData(spawnedEntity, new MoveSpeedData { moveSpeed = 5f, angle = i * (360 / weaponData.bulletCount) + weaponData.angle });

                    weaponData.angle = (weaponData.angle + weaponData.rotationSpeed / 60) % 360;
                }
            }
        });
    }
}