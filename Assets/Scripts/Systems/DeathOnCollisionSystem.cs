﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class DeathOnCollisionSystem : JobComponentSystem
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnCreate()
    {
        base.OnCreate();
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
    }

    [BurstCompile]
    struct DeathOnCollisionSystemJob : ICollisionEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<DeathColliderTag> deathColliderGroup;
        [ReadOnly] public ComponentDataFromEntity<OrbDestroyableTag> orbDestroyableGroup;
        [ReadOnly] public ComponentDataFromEntity<OrbUndestroyableTag> orbUndestroyableGroup;

        public ComponentDataFromEntity<HealthData> healthGroup;
        public void Execute(CollisionEvent collisionEvent)
        {
            Entity entityA = collisionEvent.EntityA;
            Entity entityB = collisionEvent.EntityB;

            bool entityAIsOrbDestroyable = orbDestroyableGroup.HasComponent(entityA);
            bool entityAIsOrbUndestroyable = orbUndestroyableGroup.HasComponent(entityA);
            bool entityAIsDeathCollider = deathColliderGroup.HasComponent(entityA);
            bool entityBIsOrbDestroyable = orbDestroyableGroup.HasComponent(entityB);
            bool entityBIsOrbUndestroyable = orbUndestroyableGroup.HasComponent(entityB);
            bool entityBIsDeathCollider = deathColliderGroup.HasComponent(entityB);

            if ((entityAIsOrbDestroyable || entityAIsOrbUndestroyable) && entityBIsDeathCollider)
            {
                HealthData modifiedHealth = healthGroup[entityA];
                modifiedHealth.isDead = true;
                healthGroup[entityA] = modifiedHealth;
            }
            if ((entityBIsOrbDestroyable || entityBIsOrbUndestroyable) && entityAIsDeathCollider)
            {
                HealthData modifiedHealth = healthGroup[entityB];
                modifiedHealth.isDead = true;
                healthGroup[entityB] = modifiedHealth;
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var job = new DeathOnCollisionSystemJob
        {
            deathColliderGroup = GetComponentDataFromEntity<DeathColliderTag>(true),
            orbDestroyableGroup = GetComponentDataFromEntity<OrbDestroyableTag>(true),
            orbUndestroyableGroup = GetComponentDataFromEntity<OrbUndestroyableTag>(true),
            healthGroup = GetComponentDataFromEntity<HealthData>(false)
        };

        JobHandle jobHandle = job.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDeps);
        jobHandle.Complete();

        return jobHandle;
    }
}
