﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

public class MoverSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref Translation translation, ref Rotation rotation, ref MoveSpeedData moveSpeedComponent) => {

            Vector2 dir = DegreeToVector2(moveSpeedComponent.angle);

            float3 f = new Vector3(dir.x, 0, dir.y).normalized;

            translation.Value += f * moveSpeedComponent.moveSpeed * Time.DeltaTime;
        });
    }

    public static Vector2 RadianToVector2(float radian)
    {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }
    public static Vector2 DegreeToVector2(float degree)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }
}