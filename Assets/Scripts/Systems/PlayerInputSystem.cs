﻿using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Rendering;

public class PlayerInputSystem : SystemBase
{
    protected override void OnCreate()
    {
        Entities.ForEach((ref MoveData moveData, in InputData inputData) =>
        {

        }).Run();
    }

    protected override void OnUpdate()
    {
        Entities.ForEach((ref MoveData moveData, in InputData inputData) =>
        {
            /*bool isRightKeyPressed = Input.GetKey(inputData.rightKey);
            bool isLeftKeyPressed = Input.GetKey(inputData.leftKey);
            bool isUpKeyPressed = Input.GetKey(inputData.upKey);
            bool isDownKeyPressed = Input.GetKey(inputData.downKey);

            //Convert bool to int 1:0
            moveData.direction.x = (isRightKeyPressed) ? 1 : 0;
            moveData.direction.x -= (isLeftKeyPressed) ? 1 : 0;
            moveData.direction.z = (isUpKeyPressed) ? 1 : 0;
            moveData.direction.z -= (isDownKeyPressed) ? 1 : 0;*/

            //Axis
            //moveData.direction.x = Input.GetAxis("Horizontal");
            //moveData.direction.z = Input.GetAxis("Vertical");

        }).Run(); //Run for Input, delatTime etc...
    }

    void Move(Vector2 direction)
    {
        Debug.Log("MOVE" + direction);
        /*moveData.direction.x = direction.x;
        moveData.direction.z = direction.y;*/
    }

    void Shoot()
    {
        Debug.Log("SHOOT");
    }
}
