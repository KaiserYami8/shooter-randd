﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class ReplaceTool : MonoBehaviour
{
    public GameObject prefab;

    private List<GameObject> oldChildren = new List<GameObject>();

    public void Replace()
    {
        int childCount = transform.childCount;

        var parent = Instantiate(new GameObject());

        foreach (Transform child in transform)
        {
            var o = Instantiate(prefab, parent.transform);
            o.transform.position = child.transform.position;
            o.transform.rotation = child.transform.rotation;
        }
        /*for (int i = 0; i < childCount; i++)
        {
            DestroyImmediate(transform.GetChild(0));
        }*/
    }
}

[CustomEditor(typeof(ReplaceTool))]
class DecalMeshHelperEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ReplaceTool replaceTool = (ReplaceTool)target;

        if (GUILayout.Button("REPLACE"))
        {
            replaceTool.Replace();
        }
    }
}