﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public struct MoveSpeedData : IComponentData
{
    public float moveSpeed;

    public float angle;
}