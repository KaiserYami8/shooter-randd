﻿using Unity.Entities;
using UnityEngine;
using UnityEditor;
using Unity.Collections;
using System;

[GenerateAuthoringComponent]
[Serializable]
public struct InputData : IComponentData
{
    public KeyCode upKey;
    public KeyCode downKey;
    public KeyCode rightKey;
    public KeyCode leftKey;
}