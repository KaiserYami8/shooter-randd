﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Internal;

[GenerateAuthoringComponent]
public struct WeaponData : IComponentData
{
    public float spawnTimer;
    public float rotationSpeed;
    public int bulletCount;

    public float spawnCountdown;

    public float angle;

    public Entity orbUndestroyable;
    public Entity orbDestroyable;
}