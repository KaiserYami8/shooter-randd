﻿using Unity.Entities;
using UnityEngine;

public class InputSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref MoveData moveData) =>
        {
            //Axis
            moveData.direction.x = Input.GetAxis("Horizontal");
            moveData.direction.z = Input.GetAxis("Vertical");
        });

    }
}
